---
is_hidden: true
comments: false
---

# Debugging resources for GitLab Support Engineers

- [Git](https://docs.gitlab.com/ee/topics/git/useful_git_commands.html)
- [Git Implementation Details](https://gitlab.com/gitlab-com/support/support-training/blob/master/content/git_implementation.md)
- [Linux cheat sheet](https://docs.gitlab.com/ee/administration/troubleshooting/linux_cheat_sheet.html)
- [Rails commands cheat sheet](https://docs.gitlab.com/ee/administration/troubleshooting/gitlab_rails_cheat_sheet.html)
- [Kubernetes cheat sheet](https://docs.gitlab.com/ee/administration/troubleshooting/kubernetes_cheat_sheet.html)
- [Resources](https://docs.gitlab.com/ee/administration/#troubleshooting)
- [Strace Quiz](https://gitlab.com/gitlab-com/support/support-training/blob/master/content/strace_quiz.md)
- [Test Environments](https://docs.gitlab.com/ee/administration/troubleshooting/test_environments.html)
- [Tools of the trade](https://docs.gitlab.com/ee/administration/troubleshooting/diagnostics_tools.html)
